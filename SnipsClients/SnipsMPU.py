#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import functools
import json
import requests

from hermes_python.hermes import Hermes
from hermes_python.ontology import *

class SnipsMPU(object):
    def __init__(self, i18n, mqtt_addr, site_id):
        self.THRESHOLD_INTENT_CONFSCORE_DROP = 0.3
        self.THRESHOLD_INTENT_CONFSCORE_TAKE = 0.6

        self.__i18n = i18n
        self.__site_id = site_id

        self.__mqtt_addr = mqtt_addr

    def check_site_id(handler):
        @functools.wraps(handler)
        def wrapper(self, hermes, intent_message):
            if intent_message.site_id != self.__site_id:
                return None
            else:
                return handler(self, hermes, intent_message)
        return wrapper

    def check_confidence_score(handler):
        @functools.wraps(handler)
        def wrapper(self, hermes, intent_message):
            if handler is None:
                return None
            if intent_message.intent.confidence_score < self.THRESHOLD_INTENT_CONFSCORE_DROP:
                hermes.publish_end_session(
                    intent_message.session_id,
                    ''
                )
                return None
            elif intent_message.intent.confidence_score <= self.THRESHOLD_INTENT_CONFSCORE_TAKE:
                print(intent_message.input);
                message = self.callLocalApi(intent_message.input);
                print(message);
                temperature = message["message"];
                if message["expected"] == True :
                    hermes.publish_continue_session(
                        intent_message.session_id,
                        self.__i18n.get('reply', {"message": temperature}),
                        ["tawheedkhan:fallback"],
                        custom_data=[]
                    )
                else :
                    hermes.publish_end_session(
                        intent_message.session_id,
                        self.__i18n.get('reply', {"message": temperature})
                    )
                return None
            return handler(self, hermes, intent_message)
        return wrapper

    @check_confidence_score
    @check_site_id
    def callAPI(self, hermes, intent_message):
        print("Temperature Check");
        print(intent_message.input);
        message = self.callLocalApi(intent_message.input);
        print(message);
        temperature = message["message"];
        if message["expected"] == True :
            hermes.publish_continue_session(
                intent_message.session_id,
                self.__i18n.get('reply', {"message": temperature}),
                ["tawheedkhan:fallback"],
                custom_data=[]
            )
        else :
            hermes.publish_end_session(
                intent_message.session_id,
                self.__i18n.get('reply', {"message": temperature})
            )
    
    def callLocalApi(self,message):
        api_url = '{0}bot'.format("http://localhost/");
        payload =   '{"message":"'+message+'"}';
        response = requests.post(api_url,data=payload,headers={"Content-Type":"application/json"});
        if response.status_code == 200 :
            body = json.loads(response.content.decode('utf-8'));
            if body["success"] == 1 :
                return {"message" : body["message"],"expected":body["expected"]};
            else :
                return {"message":'Please try again later.',"expected":false};
        else :
            return {"message":'Please try again later.',"expected":false};

    def start_block(self):
        with Hermes(self.__mqtt_addr) as h:
            h.subscribe_intent(
                'tawheedkhan:fallback',
                self.callAPI
            ) \
             .start()